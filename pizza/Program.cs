﻿
using pizza;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;

;

Console.OutputEncoding = System.Text.Encoding.UTF8;

List<Pizza> GetPizzasFromCode()
{
    return new List<Pizza>()
        {
            new("Margherita", 8f, true, new List<string>() { "tomate", "mozzarela", "basilic", "huile d'olive" }),
            new("Savoyarde", 9f, false, new List<string>() { "crème fraiche", "lardon", "pomme de terre" }),
            new("Reine", 9f, false, new List<string>() { "tomate", "mozzarela", "fromage", "jambon", "champignon" }),
            new("4 fromages", 10f, true, new List<string>() { "tomates", "mozzarela", "basilic", "fromage de chèvre", "roquefort", "emmental" }),
            new("chorizo", 10.5f, false, new List<string>() { "tomate", "mozzarela", "basilic", "huile d'olive", "chorizo" }),
            //new CustomPizza(),
            //new CustomPizza(),
        };
}

List<Pizza>? GetPizzasFromFile(string filename)
{
    string datasString;
    try
    {
        datasString = File.ReadAllText(filename);
    }
    catch (FileNotFoundException ex)
    {
        Console.WriteLine($"Fichier non trouvé: {ex.Message}");
        return null;
    }

    try
    {
        return JsonConvert.DeserializeObject<List<Pizza>>(datasString);
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Erreur durant la désérialisation: {ex.Message}");
        return null;
    }
}

void GenereteJsonFile(List<Pizza> pizzas, string filename)
{
    string serializedPizzas;
    try
    {
        serializedPizzas = JsonConvert.SerializeObject(pizzas);
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Erreur durant la sérialisation: {ex.Message}");
        return;
    }
    try
    {
        File.WriteAllText(filename, serializedPizzas);
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Erreur durant la génération du fichier: {ex.Message}"); ;
        return;
    }
    
}


Task GetPizzasFromUrl(string url, string filename)
{
    Console.WriteLine("coucou 1");
    return Task.Run(async () =>
    {
        try
        {
            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string s = await response.Content.ReadAsStringAsync();

            var pizzas = JsonConvert.DeserializeObject<List<Pizza>>(s);
            GenereteJsonFile(pizzas, filename);

        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine($"Erreur de type http : ${ex.StatusCode} - ${ex.Message}");
            return;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Une erreur est survenue : ${ex.Message}");
        }
    });


}



string filename = "pizzas.json";
string url = "https://codeavecjonathan.com/res/pizzas2.json";

List<Pizza>? pizzas = GetPizzasFromFile(filename);

if (pizzas is null || pizzas.Count == 0)
{
    Task t = GetPizzasFromUrl(url, filename);
    t.Wait();

    pizzas = GetPizzasFromFile(filename);

    if (pizzas is null || pizzas.Count == 0)
    {
        pizzas = GetPizzasFromCode();
    }
        
    GenereteJsonFile(pizzas, filename);
}





pizzas.OrderBy(p => p.prix)
//.Where(p => p.HaveIngretient("chorizo"))
.ToList();

foreach (var pizza in pizzas)
{
    pizza.Display();
};














