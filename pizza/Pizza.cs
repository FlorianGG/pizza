﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizza
{
    internal class Pizza
    {
        public string nom { get; protected set; }
        public float prix { get; protected set; }
        public bool vegetarienne { get; private set; }
        public List<string> ingredients { get; protected set; }
        public Pizza(string nom, float prix, bool vegetarienne, List<string> ingredients) { 
            this.nom = nom;
            this.prix = prix;
            this.vegetarienne = vegetarienne;
            this.ingredients = ingredients;
        }

        public void Display()
        {
            Console.WriteLine($"Pizza {Pizza.Capitalize(this.nom)}{(this.vegetarienne ? " (V)" : "")} - {this.prix}€");
            Console.WriteLine($"{String.Join(", ", Pizza.CapitalizeList(this.ingredients))}");
            Console.WriteLine();
        }

        private static List<string> CapitalizeList(List<string> strings)
        {
            return strings.Select(el => Pizza.Capitalize(el)).ToList();
        }

        private static string Capitalize(string s)
        {
            if (!String.IsNullOrEmpty(s))
            {
                return s[0].ToString().ToUpper() + s[1..].ToLower();
            }
            else
            {
                return s?.ToLower() ?? "";
            }

        }

        public bool HaveIngretient(string ingredient)
        {
            return this.ingredients.Where(i => i.ToLower().Contains(ingredient)).ToList().Count > 0;
        }
    }

    internal class CustomPizza : Pizza
    {
        static int nbCustomPizza = 0;
        int idCustomPizza = 0;
        public CustomPizza() : base("Personnalisée", 5f, false, new List<string>()) {
            nbCustomPizza++;
            this.idCustomPizza = nbCustomPizza;

            this.nom += $" n°{this.idCustomPizza}";
            this.AddIngretients();
            this.prix = this.calculateprix();
        }

        private float calculateprix()
        {
            return 5 + 1.5f * this.ingredients.Count;
        }

        private void AddIngretients()
        {
            while (true)
            {
                Console.WriteLine($"Choisissez un ingrédient pour votre pizza personnalisée n° {this.idCustomPizza}: ");
                if (this.ingredients.Count > 0)
                {
                    Console.WriteLine(String.Join(", ", this.ingredients));
                }
                string? ingredient = Console.ReadLine()?.Trim().ToLower();

                if (String.IsNullOrEmpty(ingredient))
                {
                    if (this.ingredients.Count == 0)
                    {
                        Console.WriteLine("Veuillez choisir au moins un ingrédient!");
                    }
                    else
                    {
                        break;
                    }
                }
                else if (this.ingredients.Contains(ingredient))
                {
                    Console.WriteLine("Cet ingrédient est déjà dans la liste");
                }
                else {
                    this.ingredients.Add(ingredient);
                }
                Console.WriteLine();
            }
        }
    }
}
